import SocketServer
from BaseHTTPServer import BaseHTTPRequestHandler
import time
from multiprocessing import Process

from neopixel import *


# LED strip configuration:
LED_COUNT      = 4080      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering
#LED_STRIP


splits = {0:{1:144, 400:500, 900:1100, 1400:1700}, 144 : {145 : 190, 190 : 399}}
cities = [23, 24, 33, 34, 39, 40, 47, 48, 60, 109, 145, 199, 222, 277, 356, 388]
STEPS = {}
LEDS = {}

def initSteps():
  i = 0
  for g in splits:
    i = i+1
    for s in splits[g]:
      if g in LEDS:
        step = LEDS[g]+1
      else:
        step = i
      for r in range(s, splits[g][s]):
        LEDS[r] = step
        step = step+1
  for l in LEDS:
    if LEDS[l] in STEPS:
      STEPS[LEDS[l]].append(l)
    else:
      STEPS[LEDS[l]] = []
      STEPS[LEDS[l]].append(l)


# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
  """Wipe color across display a pixel at a time."""
  for i in range(strip.numPixels()):
    strip.setPixelColor(i, color)
    strip.show()
    time.sleep(wait_ms/1000.0)

def theaterChase(strip, color, wait_ms=50, iterations=10):
  """Movie theater light style chaser animation."""
  for j in range(iterations):
    for q in range(3):
      for i in range(0, strip.numPixels(), 3):
        strip.setPixelColor(i+q, color)
      strip.show()
      time.sleep(wait_ms/1000.0)
      for i in range(0, strip.numPixels(), 3):
        strip.setPixelColor(i+q, 0)

def blackout(strip):
  for i in STEPS:
    for j in STEPS[i]:
      strip.setPixelColor(j, Color(0,   0,   0))
    strip.show()
    time.sleep(25/1000.0)

def allBlue(strip):
  for i in range(LED_COUNT):
    strip.setPixelColor(i, Color(0,0,255))
  strip.show()
def allOff(strip):
  for i in range(LED_COUNT):
    strip.setPixelColor(i, Color(0,0,0))
  strip.show()
def everyFive(strip):
  for i in range(LED_COUNT):
    if i%10 == 0:
      strip.setPixelColor(i, Color(255,0,0))
    #elif i%5 == 0:
    #  strip.setPixelColor(i, Color(255,0,0))
    #else:
    #  strip.setPixelColor(i, Color(0,255,0))
  strip.show()

def fanOut(strip, wait_ms=50):
  for i in STEPS:
    for j in STEPS[i]:
      if j in cities:
        strip.setPixelColor(j, Color(0,   0,   255))
      else:
        strip.setPixelColor(j, Color(0,   0,   10))
    strip.show()
    time.sleep(wait_ms/1000.0)



strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
# Intialize the library (must be called once before other functions).
strip.begin()
allOff(strip)