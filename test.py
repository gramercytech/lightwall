import SocketServer
from BaseHTTPServer import BaseHTTPRequestHandler
import time
from multiprocessing import Process

from neopixel import *


# LED strip configuration:
LED_COUNT      = 4080      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 1066000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_STRIP      = ws.WS2811_STRIP_GRB   # Strip type and colour ordering
#LED_STRIP


splits = {0: {0: 88, 230: 323, 717: 858, 1115: 1225, 1358: 1460, 1970: 2252, 2253: 2342, 2519: 2740, 2846: 2891, 3153: 3325, 3326:3494, 3495: 3590, 3816: 4000, 4001: 4090}, 
          87: {87:112, 113:229}, 
          322: {323: 340, 341: 372, 373: 409, 410: 450}, 
          857: {857: 957, 958: 1019, 1020: 1114},
          1224: {1225: 1273, 1274: 1309, 1310: 1357}, 
          1459: {1460: 1548, 1549: 1625, 1626: 1709}, 
          2341: {2342: 2420, 2421: 2467, 2468: 2518},
          2739: {2740: 2765, 2766: 2795, 2796: 2810, 2811: 2845},
          2888: {2890: 3034, 3035: 3153},
          3587: {3588: 3700, 3720: 3815}, 447: {449: 555, 556: 656, 657: 716}, 1707: {1708: 1786, 1787: 1869, 1870: 1909, 1910: 1969}
          }
splitindex = [0, 87, 322, 857, 1224, 1459, 2341, 2739, 2888, 3587, 447, 1707]
cities = [87, 324, 657, 1020, 1225, 1452, 1702, 2134, 2341, 2593, 2738, 2892, 3210, 3489, 3577, 3886]
always_on = [0, 230, 1358, 2253, 2847, 3495, 1965, 1964, 1963, 1962, 3812, 2517]
STEPS = {}
LEDS = {}
USED = []

def initSteps():
  USED = []
  i = 0 
  for g in splitindex:
    i = i+1
    for s in splits[g]:
      if g in LEDS:
        step = LEDS[g]+1
      else:
        step = i
      for r in range(s, splits[g][s]):
        LEDS[r] = step
        step = step+1
  for l in LEDS:
    if LEDS[l] in STEPS:
      STEPS[LEDS[l]].append(l)
    else:
      STEPS[LEDS[l]] = []
      STEPS[LEDS[l]].append(l)



def allBlue(strip):
  for i in range(LED_COUNT):
    strip.setPixelColor(i, Color(0,0,255))
  strip.show()
def allOff(strip):
  for i in range(LED_COUNT):
    if i in always_on:
      strip.setPixelColor(i, Color(0,0,255))
    else:
      strip.setPixelColor(i, Color(0,0,0))
  strip.show()
def everyFive(strip):
  for i in range(LED_COUNT):
    if i%8 == 0:
      strip.setPixelColor(i, Color(255,0,0))
    #elif i%5 == 0:
    #  strip.setPixelColor(i, Color(255,0,0))
    #else:
    #  strip.setPixelColor(i, Color(0,255,0))
  strip.show()

def fanOut(strip, wait_ms=50):
  USED = {}
  for i in STEPS:
    for j in STEPS[i]:
      if j in always_on:
        strip.setPixelColor(j, Color(0,0,255))
      elif j in cities:
        strip.setPixelColor(j, Color(0,   0,   255))
      else:
        strip.setPixelColor(j, Color(0,   0,   15))
    if i%8 == 0:
      strip.show()
  fadeOff(strip)
def fadeOff(strip):
  cityB = [150, 120, 110, 100, 80, 65, 50, 45, 40, 35, 30, 25, 20, 10, 5]
  for k in range (20):
    for r in cities:
      strip.setPixelColor(r, Color(0,0,250-(k*5)))
    strip.show()
  for j in range(15):
    for i in range(LED_COUNT):
      if i in always_on:
        strip.setPixelColor(i, Color(0,0,255))
      elif i in cities:
        strip.setPixelColor(i, Color(0,0,cityB[j]))
      else:
        strip.setPixelColor(i, Color(0,0,15-j))
    strip.show()
    time.sleep(1/4)
  for i in range(LED_COUNT):
    if i in always_on:
        strip.setPixelColor(i, Color(0,0,255))
    else:
      strip.setPixelColor(i, Color(0,0,0))
  strip.show()


    


class MyHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        if self.path == '/send':
            #fanOut(strip)
            #blackout(strip)
            #allBlue(strip)
            
            everyFive(strip)
        self.send_response(200)

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL, LED_STRIP)
# Intialize the library (must be called once before other functions).
strip.begin()
allOff(strip)
initSteps()
fanOut(strip)