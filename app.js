const express = require('express')
const app = express();

app.use(express.static('public'))

app.get('/', (req, res) => res.send('Hello World!'))

process.on('SIGINT', function () {
  ws281x.reset();
  process.nextTick(function () { process.exit(0); });
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))